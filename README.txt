INTRODUCTION
------------
This module allows importing of menu links using Feeds.


REQUIREMENTS
------------
This module requires the following modules:
* Feeds (https://drupal.org/project/feeds)


INSTALLATION
-------------
* Install as usual, see http://drupal.org/node/895232 for further information.


USAGE
---------------
* Go to admin/structure/feeds and add a new importer
* Select the "Menu processor"
* In Settings, select a menu to use to create new menu links
* In Mapping, select at least path and title


MAINTAINERS
---------------
* Roman Polishchuk (darth romios) - https://drupal.org/user/2771673
